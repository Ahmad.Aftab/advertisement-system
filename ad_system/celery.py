import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ad_system.settings')

app = Celery('ad_system')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()
