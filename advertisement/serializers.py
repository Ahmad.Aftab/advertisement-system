from rest_framework import serializers
from .models import Ad, Location

class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Location
        fields = '__all__'

class AdSerializer(serializers.ModelSerializer):
    locations = LocationSerializer(many=True, read_only=True)

    class Meta:
        model = Ad
        fields = ('id', 'name', 'start_date', 'end_date', 'locations')
