from celery import shared_task
from .models import AdLocation,Ad
import datetime

@shared_task
def reset_visitor_counts_and_check_expiration():
    AdLocation.objects.all().update(daily_visitors=0, blocked=False)
    expired_ads = Ad.objects.filter(end_date__lte=datetime.date.today())
    AdLocation.objects.filter(ad__in=expired_ads).update(blocked=True)
