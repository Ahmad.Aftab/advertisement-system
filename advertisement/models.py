from django.db import models

class Location(models.Model):
    name = models.CharField(max_length=100)
    max_visitors = models.PositiveIntegerField()

    def __str__(self):
        return self.name

class Ad(models.Model):
    name = models.CharField(max_length=255)
    start_date = models.DateField()
    end_date = models.DateField()
    locations = models.ManyToManyField(Location, through='AdLocation')

    def __str__(self):
        return self.name

class AdLocation(models.Model):
    ad = models.ForeignKey(Ad, on_delete=models.CASCADE)
    location = models.ForeignKey(Location, on_delete=models.CASCADE)
    daily_visitors = models.PositiveIntegerField(default=0)
    blocked = models.BooleanField(default=False)
    
    def save(self, *args, **kwargs):
        if self.daily_visitors >= self.location.max_visitors:
            self.blocked = True
        super(AdLocation, self).save(*args, **kwargs)
