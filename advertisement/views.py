from rest_framework import viewsets
from .models import Ad, Location, AdLocation
from .serializers import AdSerializer
from django.http import JsonResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response

class AdViewSet(viewsets.ModelViewSet):
    queryset = Ad.objects.all()
    serializer_class = AdSerializer

    def get_queryset(self):
        location_code = self.request.query_params.get('location', None)
        if location_code:
            location = Location.objects.get(name__iexact=location_code)
            return Ad.objects.filter(locations__location=location, locations__blocked=False)
        return Ad.objects.all()

@api_view(['POST'])
def increment_visitor_count(request, ad_location_id):
    try:
        ad_location = AdLocation.objects.get(pk=ad_location_id)
        if ad_location.blocked:
            return Response({"error": "Ad is blocked in this location"}, status=400)
        ad_location.daily_visitors += 1
        ad_location.save()
        return Response({"success": "Visitor count incremented"})
    except AdLocation.DoesNotExist:
        return Response({"error": "Ad Location not found"}, status=404)
