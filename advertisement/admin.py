from django.contrib import admin
from .models import Ad, Location, AdLocation

admin.site.register(Ad)
admin.site.register(Location)
admin.site.register(AdLocation)
