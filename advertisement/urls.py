from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import AdViewSet, increment_visitor_count

router = DefaultRouter()
router.register(r'ads', AdViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('increment-visitor/<int:ad_location_id>/', increment_visitor_count, name='increment-visitor'),
]
